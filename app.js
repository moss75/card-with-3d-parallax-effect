window.onload = () => {
    let card = document.querySelector('.card');
    let top = document.querySelector('.top');
    let body = document.querySelector('body');
    let filter = document.querySelector('.filter');
    let thumb = document.querySelector('.thumb');
    let player = document.querySelector('#player');
    thumb.addEventListener('mouseenter', function(z) {
        player.pause()
    })
    top.addEventListener('mousemove', function(t) {
        let e = -((window.innerWidth / 2 - t.pageX) / 30)
        let n = -((window.innerHeight / 2 - t.pageY) / 30)

        card.style.transform = `rotateY(${e}deg) rotateX(${n}deg)`
    })
}

